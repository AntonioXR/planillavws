var empleado = require('../model/empleado'), router = require('express').Router()

//Operacion para insertar un empleado
router.post('/empleado',(req, res)=>{
    var data = [req.body.nombre, req.body.apellido, req.body.estado, req.body.fecha_inicio, req.body.fecha_inactividad, req.body.idAdministrador, req.body.bonificacion, req.body.isr]
    empleado.insert(data, resp=>{
        res.json(resp)
    })
})
//Obtener todos los empleados de un administrador
router.get('/empleado/ad/:id',(req,res)=>{
        empleado.selectAll(req.params.id, (resp)=>{
            res.json(resp)
        })
})

router.route('/empleado/:id')
//Mostrar solo un empleado
    .get((req,res)=>{
        empleado.select(req.params.id, resp=>{
            res.json(resp)
        })
    })
//Editar un empleado
    .put((req, res)=>{
        var data = [req.body.nombre, req.body.apellido, req.body.fecha_inicio,
            (req.body.idEstado == 2 )?req.body.fecha_inactividad : null , 
            req.body.idEstado,req.body.idAdministrador, req.params.id]
        empleado.update(data, (resp)=>{
            res.json(resp)
        })
    })
//Eliminar un empleado
    .delete((req,res)=>{
        empleado.delete(req.params.id, (resp)=>{
            empleado.delete(req.params.id, resp=>{
                res.json(resp)
            })
        })
    })
    

module.exports = router