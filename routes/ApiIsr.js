var isr = require('../model/isr'), router = require('express').Router()

router.get('/isr/em/:id', (req, res)=>{
    isr.selectAll(req.params.id, resp=>{
        res.json(resp)
    })
})

router.post('/isr/',(req, res)=>{
    var data = [req.body.cuota, req.body.mes, req.body.anio , req.body.idEmpleado]
    isr.insert(data, resp=>{
        res.json(resp)
    })
})

router.route('/isr/:id')
    .get((req, res)=>{
        isr.select(req.params.id, resp=>{
            res.json(resp)
        })
    })
    .delete((req, res)=>{
        isr.delete(req.params.id, resp=>{
            res.json(resp)
        })
    })

module.exports = router