var salario = require('../model/salario') , router = require('express').Router()

/**
 * Se da una ruta para ambos metodos HTTP
 */

router.route('/salario')
    .get((req, res, next)=>{
        salario.selectAll((response)=>{
            res.json(response)
        })
    })
    .post((req, res)=>{
        var data = [req.body.cuota, req.body.anio ]
        salario.insert(data,(response)=>{
            res.json(response)
        })
    })

router.route('/salario/:id')
    .put((req, res)=>{
        var data = [req.body.cuota, req.body.anio, req.params.id]
        salario.update(data,(response)=>{
            res.json(response)
        })
    })
    .delete((req, res)=>{
        salario.delete(req.params.id,(response)=>{
            res.json(response)
        })
    })
    .get((req, res)=>{
        salario.select(req.params.id, resp=>{
            res.json(resp)
        })
    })

router.get('/salario/anio/:year', (req, res)=>{
    salario.forYear(req.params.year, resp=>{
        res.json(resp)
    })
})

module.exports = router