var bonificacion = require('../model/bonificacion'), router = require('express').Router()

router.get('/bonificacion/em/:id', (req, res)=>{
    bonificacion.selectAll(req.params.id, resp=>{
        res.json(resp)
    })
})

router.post('/bonificacion/',(req, res)=>{
    var data = [req.body.cuota, req.body.mes, req.body.anio , req.body.idEmpleado]
    bonificacion.insert(data, resp=>{
        res.json(resp)
    })
})

router.route('/bonificacion/:id')
    .get((req, res)=>{
        bonificacion.select(req.params.id, resp=>{
            res.json(resp)
        })
    })
    .delete((req, res)=>{
        bonificacion.delete(req.params.id, resp=>{
            res.json(resp)
        })
    })

module.exports = router