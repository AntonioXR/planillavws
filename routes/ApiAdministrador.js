var adm = require('../model/administrador') , router = require('express').Router()

/**
 * Se da una ruta para ambos metodos HTTP
 */

router.post('/admin/log',(req, res, next)=>{
    var data = [req.body.nick, req.body.contrasena] ;
        adm.login(data,(response)=>{
            res.json(response)
        })
    })

router.put('/admin/:id',(req, res)=>{
        var data = [req.body.nombre , req.body.apellido, req.body.contrasena, req.params.id]
        adm.update(data,(response)=>{
            res.json(response)
        })
    })

router.post('/admin/',(req, res)=>{
    var data = [req.body.nombre , req.body.apellido, req.body.nick ,req.body.contrasena]
    adm.insert(data,(response)=>{
        res.json(response)
    })
})

module.exports = router