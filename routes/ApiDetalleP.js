var detalleP = require('../model/detalleplanilla') , router = require('express').Router()

router.route('/detalleP/:id')
    .get((req, res)=>{
        detalleP.select(req.params.id, resp=>{
            res.json(resp)
        })
    })
    .put((req, res)=>{
        var data = [ req.body.bonificacion, req.body.retencion_isr, req.body.salario_liquido , req.body.salario_base, req.body.igss, req.body.sueldo_ordinario, req.params.id]
        detalleP.update(data, resp=>{
            res.json(resp)
        })
    })
    .delete((req, res)=>{
        detalleP.delete(req.params.id, resp=>{
            res.json(resp)
        })
    })
    
router.get('/detalleP/pla/:id', (req, res)=>{
        detalleP.selectAll(req.params.id, resp=>{
            res.json(resp)
        })
})

router.get('/detalleP/pla/total/:id', (req, res)=>{
    detalleP.selectTotales(req.params.id, resp=>{
        res.json(resp)
    })
})

module.exports = router