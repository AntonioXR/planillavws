var igss = require('../model/igss') , router = require('express').Router()

/**
 * Se da una ruta para ambos metodos HTTP
 */

router.route('/igss')
    .get((req, res, next)=>{
        igss.selectAll((response)=>{
            res.json(response)
        })
    })
    .post((req, res)=>{
        var data = [req.body.anio ,req.body.cuota]
        igss.insert(data,(response)=>{
            res.json(response)
        })
    })

router.route('/igss/:id')
    .put((req, res)=>{
        var data = [req.body.anio ,req.body.cuota, req.params.id]
        igss.update(data,(response)=>{
            res.json(response)
        })
    })
    .delete((req, res)=>{
        igss.delete(req.params.id,(response)=>{
            res.json(response)
        })
    })    
    .get((req, res)=>{
        igss.select(req.params.id, resp=>{
            res.json(resp)
        })
    })

router.get('/igss/anio/:year', (req, res)=>{
    igss.forYear(req.params.year, resp=>{
        res.json(resp)
    })
})
module.exports = router