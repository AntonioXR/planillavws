var planilla = require('../model/planilla'), router = require('express').Router()

router.get('/planilla/adm/:id', (req, res)=>{
    planilla.selectAll(req.params.id, resp=>{
        res.json(resp)
    })
})

router.post('/planilla', (req, res)=>{
    var data = [parseInt(req.body.mes), parseInt(req.body.anio), req.body.comentario, parseInt(req.body.idAdministrador) ]
    planilla.insert(data, resp=>{
        res.json(resp)
    })
})

router.route('/planilla/:id')
    .get((req, res)=>{
        planilla.select(req.params.id, resp=>{
            res.json(resp)
        })
    })
    .delete((req, res)=>{
        planilla.delete(req.params.id, resp=>{
            res.json(resp)
        })
    })
    .put((req, res)=>{
        var data = [req.body.comentario, parseInt(req.params.id) ]
        planilla.update(data, resp=>{
            res.json(resp)
        })
    })

router.get('/planilla/:mes/:anio', (req, res)=>{
    var data = [req.params.mes,req.params.anio]
    planilla.selectByMonth(data, resp=>{
        res.json(resp)
    })
})

router.get('/planilla/adm/:id/:mes/:anio', (req, res)=>{
    var data = [req.params.mes,req.params.anio, req.params.id]
    planilla.previo(data, resp=>{
        res.json(resp[0])
    })
})

router.get('/planilla/adm/:id/anio', (req, res)=>{
    planilla.getAnio(req.params.id, resp=>{
        res.json(resp)
    })
})

router.get('/planilla/:id/anio/:anio', (req, res)=>{
    planilla.getByYear([req.params.anio,req.params.id], resp=>{
        res.json(resp)
    })
})


module.exports = router