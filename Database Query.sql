CREATE DATABASE Planilla_Viaro;
USE Planilla_Viaro;

CREATE TABLE adminstrador(
    idAdministrador INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(60) NOT NULL,
    apellido VARCHAR(60) NOT NULL,
    nick VARCHAR(20) NOT NULL,
    contrasena VARCHAR(20) NOT NULL
);

CREATE TABLE estado(
    idEstado INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    estado VARCHAR(20) NOT NULL
);

CREATE TABLE igss(
    idIGSS INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    anio INT NOT NULL,
    cuota DECIMAL(5,2) NOT NULL
);

CREATE TABLE salario(
    idSalario INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    anio INT NOT NULL,
    cuota DECIMAL(5,2) NOT NULL
);

CREATE TABLE empleado(
    idEmpleado INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    apellido VARCHAR(50) NOT NULL,
    fecha_inicio DATETIME,
    fecha_inactividad DATETIME,
    idEstado INT NOT NULL,
    idAdministrador INT NOT NULL,
    FOREIGN KEY (idEstado) REFERENCES estado(idEstado),
    FOREIGN KEY (idAdministrador) REFERENCES adminstrador(idAdministrador)
);

CREATE TABLE isr (
    idISR INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    cuota DECIMAL(5,2) NOT NULL,
    fecha_creacion DATETIME NOT NULL,
    idEmpleado INT NOT NULL,
    FOREIGN KEY (idEmpleado) REFERENCES empleado(idEmpleado)
);

CREATE TABLE bonificacion(
    idBonificacion INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    cuota DECIMAL(5,2) NOT NULL,
    fecha_creacion DATETIME,
    idEmpleado INT NOT NULL,
    FOREIGN KEY (idEmpleado) REFERENCES empleado(idEmpleado)
);

CREATE TABLE planilla(
    idPlanilla INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    fecha_creacion DATETIME NOT NULL,
    mes INT NOT NULL,
    anio INT NOT NULL,
    comentario TEXT,
    idAdministrador INT NOT NULL,
    FOREIGN KEY (idAdministrador) REFERENCES adminstrador(idAdministrador)  
);

CREATE TABLE DetallePlanilla(
    idDetalleP INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    idEmpleado INT NOT NULL,
    nombre_empleado VARCHAR(100),
    /* estado_empleado: 1 - Existente / 2 - Borrado */ 
    estado_empleado INT NOT NULL,
    idPlanilla INT,
    bonificacion DECIMAL(5,2) NOT NULL,
    retencion_isr DECIMAL(5,2) NOT NULL,
    salario_liquido DECIMAL(5,2) NOT NULL,
    salario_base DECIMAL(5,2) NOT NULL,
    igss DECIMAL(5,2) NOT NULL,
    FOREIGN KEY (idPlanilla) REFERENCES planilla(idPlanilla)
);

DELIMITER $$

CREATE PROCEDURE sp_agregarEmpleado(
    IN _nombre VARCHAR(50),
    IN _apellido VARCHAR(50),
    IN _estado INT,
    IN _fecha_creacion DATETIME,
    IN _fecha_inactividad DATETIME,
    IN _idAdministrador INT,
    IN _bonificacion DECIMAL(5,2),
    IN _isr DECIMAL(5,2)
)
BEGIN 
    DECLARE _idEmpleado INT;
    /**
    *Se revisa si el usuario no fue puesto en estado Inactivo desde su creacion
    *de ser 1 el estado, no se guarda la fecha de Inactividad, ya que esta ACTIVO
    *De no ser asi, guarda la fecha.
     */
    IF (_estado = 1) THEN 
        INSERT INTO empleado(nombre, apellido, fecha_inicio,  idEstado, idAdministrador)
        VALUES(_nombre, _apellido, _fecha_creacion, _estado, _idAdministrador);
        
        SET _idEmpleado =  (SELECT idEmpleado FROM empleado WHERE nombre = _nombre && apellido = _apellido && fecha_inicio = _fecha_creacion ORDER BY fecha_inicio DESC LIMIT 1);
        
        INSERT INTO bonificacion(cuota, fecha_creacion,idEmpleado)
        VALUES(_bonificacion, NOW(), _idEmpleado); 
        
        INSERT INTO isr(cuota, fecha_creacion, idEmpleado)
        VALUES(_isr, NOW(), _idEmpleado);
    ELSE
        INSERT INTO empleado(nombre, apellido, fecha_creacion,fecha_inactividad  ,idEmpleado, idAdministrador)
        VALUES(_nombre, _apellido, _fecha_creacion,_fecha_inactividad ,_estado, _idAdministrador);
        
        SET _idEmpleado =  (SELECT idEmpleado FROM empleado WHERE nombre = _nombre && apellido = _apellido && fecha_creacion = _fecha_creacion ORDER BY fecha_creacion DESC LIMIT 1);
        
        INSERT INTO bonificacion(cuota, fecha_creacion,idEmpleado)
        VALUES(_bonificacion, NOW(), _idEmpleado); 
        
        INSERT INTO isr(cuota, fecha_creacion, idEmpleado)
        VALUES(_isr, NOW(), _idEmpleado);
    END IF;

END $$

CREATE PROCEDURE sp_eliminarEmpleado(
    IN _idEmpleado INT
)
BEGIN
    DELETE FROM bonificacion WHERE idEmpleado = _idEmpleado;
    DELETE FROM isr WHERE idEmpleado = _idEmpleado;
    UPDATE DetallePlanilla SET estado_empleado = 2 WHERE idEmpleado = _idEmpleado; 
    DELETE FROM empleado WHERE idEmpleado =  _idEmpleado;
END $$


CREATE PROCEDURE sp_agregarPlanilla(  IN _mes INT , IN _anio INT, IN _comentario TEXT , IN _idAdministrador INT) 

BEGIN
	DECLARE _idPlanilla INT;
	
	INSERT INTO planilla(fecha_creacion, mes, anio, comentario, idAdministrador)
	VALUES (NOW() , _mes, _anio, _comentario , _idAdministrador);
	
	SET _idPlanilla = (SELECT idPlanilla FROM planilla WHERE mes = _mes && anio = _anio);

		INSERT INTO detalleplanilla(idEmpleado,nombre_empleado, estado_empleado, idPlanilla, bonificacion, retencion_isr, salario_liquido, salario_base, igss)
		
		SELECT em.idEmpleado, em.nombre AS nombre_empleado , (1) AS estado_empleado, 
		
		(_idPlanilla) AS idPlanilla, bo.cuota AS bonificacion , _isr.cuota  AS retencion_isr,
		
		(SELECT (sa.cuota - bo.cuota - _isr.cuota ) FROM salario sa WHERE anio = _anio) AS salario_liquido ,
		
		(SELECT cuota FROM salario WHERE anio = _anio) AS salario_base,  
		
		(SELECT cuota FROM igss WHERE anio = _anio) AS igss
		
		FROM empleado em 
		
		INNER JOIN (SELECT * FROM bonificacion WHERE fecha_creacion IN (SELECT MAX(fecha_creacion) FROM bonificacion 
		WHERE DATE_FORMAT(fecha_creacion, '%Y') <= _anio && DATE_FORMAT(fecha_creacion, '%m') <= _mes GROUP BY idEmpleado)) bo ON bo.idEmpleado = em.idEmpleado

		INNER JOIN (SELECT * FROM isr WHERE fecha_creacion IN (SELECT MAX(fecha_creacion) FROM isr 
		WHERE DATE_FORMAT(fecha_creacion, '%Y') <= _anio && DATE_FORMAT(fecha_creacion, '%m') <= _mes GROUP BY idEmpleado)) _isr ON _isr.idEmpleado = em.idEmpleado
		
		&& DATE_FORMAT(em.fecha_inicio, '%m') <= _mes && DATE_FORMAT(em.fecha_inicio, '%Y') <= _anio && em.idEstado != 2
		
		GROUP BY bo.idEmpleado,_isr.idEmpleado ;

END $$


CREATE PROCEDURE sp_vistaPreviaPlanilla(  IN _mes INT , IN _anio INT, IN _idAdmin INT) 

BEGIN

SELECT em.idEmpleado, em.nombre AS nombre_empleado ,
		
		 bo.cuota AS bonificacion , _isr.cuota  AS retencion_isr,
		
		(SELECT (sa.cuota - bo.cuota - _isr.cuota ) FROM salario sa WHERE anio = 2017) AS salario_liquido ,
		
		(SELECT cuota FROM salario WHERE anio = 2017) AS salario_base,  
		
		(SELECT cuota FROM igss WHERE anio = 2017) AS igss
		
		FROM empleado em 
		
		INNER JOIN (SELECT * FROM bonificacion WHERE fecha_creacion IN (SELECT MAX(fecha_creacion) FROM bonificacion 
		WHERE DATE_FORMAT(fecha_creacion, '%Y') <= 2017 && DATE_FORMAT(fecha_creacion, '%m') <= 9 GROUP BY idEmpleado)) bo ON bo.idEmpleado = em.idEmpleado

		INNER JOIN (SELECT * FROM isr WHERE fecha_creacion IN (SELECT MAX(fecha_creacion) FROM isr 
		WHERE DATE_FORMAT(fecha_creacion, '%Y') <= 2017 && DATE_FORMAT(fecha_creacion, '%m') <= 9 GROUP BY idEmpleado)) _isr ON _isr.idEmpleado = em.idEmpleado
		
		&& DATE_FORMAT(em.fecha_inicio, '%m') <= 9 && DATE_FORMAT(em.fecha_inicio, '%Y') <= 2017 && em.idEstado != 2 && em.idAdministrador = _idAdmin
		
		GROUP BY bo.idEmpleado,_isr.idEmpleado ;
		
END $$

CREATE PROCEDURE sp_eliminarPlanilla(IN _idPlanilla INT)
BEGIN
	DELETE FROM detalleplanilla WHERE idPlanilla = _idPlanilla;
	DELETE FROM planilla WHERE idPlanilla = _idPlanilla;

END $$