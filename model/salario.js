var db =  require('./database'), salario = {}

salario.selectAll = (callback)=>{
    db.query('SELECT *, DATE_FORMAT(fecha_creacion, "%Y-%m-%d" ) AS fecha_f FROM salario ', (err, result)=>{
        if(err){
            callback([])
            throw err
        }else{
            callback(result)
        }
    })
}

salario.select = (ID,callback)=>{
    db.query('SELECT *, DATE_FORMAT(fecha_creacion, "%Y-%m-%d" ) AS fecha_f FROM salario WHERE idSalario = ?',ID, (err, result)=>{
        if(err){
            callback([])
            throw err
        }else{
            callback(result)
        }
    })
}

salario.forYear = (year,callback)=>{
    db.query('SELECT * , DATE_FORMAT(fecha_creacion, "%Y-%m-%d" ) AS fecha_f FROM salario WHERE anio = ?', year, (err, result)=>{
        if(err){
            callback([])
            throw err
        }else{
            callback(result)
        }
    })
}

salario.insert = (data, callback)=>{
    db.query('INSERT INTO salario(cuota,anio, fecha_creacion) VALUES (?,?, NOW());',data, (err, result)=>{
        if(err){
            callback({Result: false, Mensaje: 'No se completo la operacion'})
            throw err
        }else{
            callback({Result: true, Mensaje:"Listo"})
        }
    })
}

salario.update = (data, callback)=>{
    db.query('UPDATE salario SET cuota=? , anio = ? WHERE idSalario = ?;',data, (err, result)=>{
        if(err){
            callback({Result: false, Mensaje: 'No se completo la operacion'})
            throw err
        }else{
            callback({Result: true, Mensaje:"Listo"})
        }
    })
}

salario.delete =  (idSalario, callback)=>{
    db.query('DELETE FROM salario WHERE idSalario = ?;',idSalario, (err, result)=>{
        if(err){
            callback({Result: false, Mensaje: 'No se completo la operacion'})
            throw err
        }else{
            callback({Result: true, Mensaje:"Listo"})
        }
    })
}

module.exports = salario