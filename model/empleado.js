var database = require('./database'), empleado = {}

/**
 * Obtener empleado de un Administrador
 * @param {number} IDA - ID Administrador
 */
empleado.selectAll = (IDA, callback)=>{
    database.query(`SELECT em.*, DATE_FORMAT(em.fecha_inactividad, '%Y-%m-%d') AS fecha_f_format  ,DATE_FORMAT(em.fecha_inicio, '%Y-%m-%d')
                     AS fecha_i_format ,  es.estado FROM empleado em 
                    INNER JOIN estado es ON es.idEstado = em.idEstado 
                    WHERE em.idAdministrador = ? ORDER BY em.idEmpleado DESC`, IDA, (err, result)=>{
        if(err){
            callback({Mensaje: 'Error de consulta', Result: false})
            throw err
        }else{
            callback(result)
        }
    })
}
/**
 * Obtener datos de un solo empleado
 * @param {Number} IDE - ID Empleado 
 * @param {Function} callback - ({Array} Datos)
 */
empleado.select = (IDE, callback)=>{
    database.query(`SELECT em.*, bn.fecha_creacion, bn.cuota AS bonificacion, isr.fecha_creacion , isr.cuota AS isr, DATE_FORMAT(em.fecha_inactividad, '%Y-%m-%d') AS fecha_f_format  , 
                    DATE_FORMAT(em.fecha_inicio, '%Y-%m-%d') AS fecha_i_format FROM empleado em
                    INNER JOIN (SELECT * FROM bonificacion WHERE fecha_creacion IN (SELECT MAX(fecha_creacion) AS fecha_creacion  FROM bonificacion GROUP BY idEmpleado)) bn 
                    ON bn.idEmpleado =  em.idEmpleado
                    INNER JOIN (SELECT * FROM isr WHERE fecha_creacion IN (SELECT MAX(fecha_creacion) AS fecha_creacion  FROM isr GROUP BY idEmpleado)) AS isr 
                    ON isr.idEmpleado = em.idEmpleado 
                    WHERE em.idEmpleado = ?`, IDE, (err, result)=>{
        if(err){
            callback({Mensaje: 'Error de consulta', Result: false})
            throw err
        }else{
            callback(result)
        }
    })
}
/**
 * Agregar un nuevo empleado
 * @param {Array} data - [nombre, apellido, estado, fecha_creacion, fecha_inactividad, idAdministrador, bonificacion, ISR]
 */
empleado.insert = (data, callback)=>{
    database.query('call sp_agregarEmpleado(?,?,?,?,?,?,?,?);', 
    data, (err, result)=>{
        if(err){
            callback({Mensaje: 'Error al Agregar Empleado', Result: false})
            throw err
        }else{
            callback({Mensaje: 'Agregado!', Result: true})
        }
    })
}

/**
 * @param {Array} data - [nombre, apellido, fecha_inicio, fecha_inactividad, idEstado, idAdministrador]
 */
empleado.update = (data, callback)=>{
    database.query(`UPDATE empleado SET nombre = ?, apellido = ?, fecha_inicio = ?,
     fecha_inactividad = ?, idEstado = ?,idAdministrador = ? WHERE idEmpleado = ?`, data, (err, result)=>{
            if(err){
                callback({Mensaje: 'Error al editar', Result: false})
                throw err
            }else{
                callback({Mensaje: 'Editado!', Result: true})
            }  
     })
}

empleado.delete = (data, callback)=>{
    database.query(`CALL sp_eliminarEmpleado(?);`, data, (err, result)=>{
            if(err){
                callback({Mensaje: 'Error al eliminar', Result: false})
                throw err
            }else{
                callback({Mensaje: 'Eliminado!', Result: true})
            }  
     })
}


module.exports = empleado