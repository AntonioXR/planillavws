var database = require('./database'), planilla = {}

/**
 * @param {number} IDA - ID Administrador
 * Obtiene todas las planillas generadas por cada administrador
 */ 
planilla.selectAll = (IDA, callback)=>{
    database.query(`SELECT pl.*, DATE_FORMAT(fecha_creacion , '%d/%m/%Y') AS fecha_f, COUNT(dp.idDetalleP) AS total_detalle, SUM(dp.salario_liquido) AS total_planilla FROM planilla pl
                    INNER JOIN detalleplanilla dp ON dp.idPlanilla = pl.idPlanilla
                    WHERE pl.idAdministrador = ?  GROUP BY dp.idPlanilla`, IDA, (err, result)=>{
            if(err){
                callback({Mensaje: 'Hubo un error al obtener los datos', Result: false})
                throw err
            }else{
                callback(result)
            }
    })
}
/** @param {number} IDP - ID Planilla
 * Seleciona solamente una planilla
 */
planilla.select = (IDP, callback)=>{
    database.query(`SELECT pl.*, COUNT(dp.idDetalleP) AS total_detalle FROM planilla pl
                    INNER JOIN detalleplanilla dp ON dp.idPlanilla = pl.idPlanilla
                    WHERE pl.idPlanilla = ?`, IDP, (err, res)=>{
            if(err){
                callback({Mensaje: 'Error al obtener el dato', Result: false})
                throw err
            }else{
                callback(res)
            }
    })
}
/** @param {Array} data - [mes, anio, idAdmin]
 * Seleciona solamente una planilla
 */
planilla.previo = (data, callback)=>{
    database.query(`CALL sp_vistaPreviaPlanilla(?, ? , ?);`, data, (err, res)=>{
            if(err){
                callback({Mensaje: 'Error al obtener el dato', Result: false})
                throw err
            }else{
                callback(res)
            }
    })
}

/**
 * @param {Array} - data [mes, anio]
 * Obtiene la planilla de un mes y anio especifico
 */
planilla.selectByMonth = (data, callback)=>{
    database.query(`SELECT pl.*, COUNT(dp.idDetalleP) AS total_detalle FROM planilla pl
                    INNER JOIN detalleplanilla dp ON dp.idPlanilla = pl.idPlanilla
                    WHERE pl.mes = ? && pl.anio = ? GROUP BY dp.idPlanilla`, data, (err, res)=>{
        if(err){
            callback({Mensaje: "Error al obtener datos", Result:false})
            throw err
        }else{
            callback(res)
        }
    })
}


planilla.getByYear = (data, callback)=>{
    database.query(`SELECT pl.*, DATE_FORMAT(fecha_creacion , '%d/%m/%Y') AS fecha_f, COUNT(dp.idDetalleP) AS total_detalle, SUM(dp.salario_liquido) AS total_planilla FROM planilla pl
                    INNER JOIN detalleplanilla dp ON dp.idPlanilla = pl.idPlanilla
                    WHERE pl.anio = ? && pl.idAdministrador = ? GROUP BY dp.idPlanilla`, data, (err, res)=>{
        if(err){
            callback({Mensaje: "Error al obtener datos", Result:false})
            throw err
        }else{
            callback(res)
        }
    })
}

/**
 * @param {Array} data - [mes, anio, comentario, idAdministrador]
 * Agrega una nueva planilla  
 */
planilla.insert = (data, callback)=>{
    database.query('CALL sp_agregarPlanilla (?,?,?,?);', data, (err, res)=>{
        if(err){
            callback({Mensaje: 'Error al insertar la nueva planilla', Result: false})
            throw err
        }else{
            callback({Mensaje: 'Planilla generada con exito!', Result: true})
        }
    })
}
/**
 * @param {Array} data - [mes, anio, comentario, idAdministrador]
 * Agrega una nueva planilla  
 */
planilla.update = (data, callback)=>{
    database.query('UPDATE planilla SET comentario = ? WHERE idPlanilla = ?;', data, (err, res)=>{
        if(err){
            callback({Mensaje: 'Error al insertar la nueva planilla', Result: false})
            throw err
        }else{
            callback({Mensaje: 'Planilla editada con exito!', Result: true})
        }
    })
}

/**
 * @param {number} IDP - ID Planilla
 * Elimina una planila enviando el ID a eliminar
 */
planilla.delete = (IDP, callback)=>{
    database.query('CALL sp_eliminarPlanilla(?);',IDP, (err, res)=>{
        if(err){
            callback({Mensaje: 'Error al eliminar la planilla', Result: false})
            throw err
        }else{
            callback({Mensaje: 'Eliminada!', Result: true})
        }
    })
}

planilla.getAnio= (IDA, callback)=>{
    database.query('SELECT DISTINCT anio FROM planilla WHERE idAdministrador = ?', IDA, (err, resp)=>{
        if(err){
            callback({Mensaje: 'Error al obtener el dato', Result: false})
            throw err
        }else{
            callback(resp)
        }
    })
}


module.exports = planilla