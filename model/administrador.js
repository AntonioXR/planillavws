var database = require('./database'), administrador = {}

/**
 * @param {Array} data - [nick, contrasena]
 * Metodo para iniciar sesion
 */

administrador.login =  (data, callback)=>{
    database.query('SELECT * FROM adminstrador WHERE nick = ? && contrasena = ?', data,(err, result)=>{
        if(err || !result){
            callback({Result: false, Mensaje: "Error de consulta"})
            throw err
        }else{
            if(result.length == 1 ){
                if(result[0].nick == data[0] && result[0].contrasena == data[1]){
                    let datos = result[0]
                    datos.Result = true
                    datos.Mensaje = 'Ingreso Correcto!'
                    datos.location = ''
                    callback(datos)
                }else{
                    callback({Result: false, Mensaje: "El Usuario o Contrasena no son correctos"})
                }
            }else{
                callback({Result: false, Mensaje: "No se encontro ningun administrador con su usuario"})
            }
        }
    })
}

administrador.insert = (data, callback)=>{
    database.query('INSERT INTO adminstrador(nombre, apellido, nick, contrasena) VALUES (?,?,?,?);', data, (err, result)=>{
        if(err){
            callback({Mensaje:'Error al registrarse!', Result:false})
            throw err
        }else{
            callback({Mensaje:'Listo!', Result:true})
        }
    })
}

/**
 * @param {Array} data - [nombre, apellido, contrasena, idAdministrador]
 * Metodo para administrar su perfil
 */

administrador.update = (data,callback)=>{
    database.query('UPDATE adminstrador SET nombre=? , apellido=?, contrasena=? WHERE idAdministrador = ?;', 
    data, (err,result)=>{
        if(err){
            callback({Result: false, Mensaje: "Error de consulta"})
            throw err
        }else{
            callback({Result: true, Mensaje: "Se modifico correctamente"})
        }
    })
}

module.exports = administrador