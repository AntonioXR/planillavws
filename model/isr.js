var database = require('./database'), isr = {}

isr.selectAll = (IDE, callback) =>{
    database.query('SELECT *, DATE_FORMAT(fecha_creacion, "%Y-%m-%d") AS fecha_f  FROM isr WHERE idEmpleado = ? ;', IDE, (err, result)=>{
        if(err){
            callback({Mensaje: 'Error al Mostrar datos', Result: false})
            throw err
        }else{
            callback(result)
        }
    })
}

isr.select = (IDB, callback)=>{
    database.query('SELECT * , DATE_FORMAT(fecha_creacion, "%Y-%m-%d") AS fecha_f FROM isr WHERE idISR = ? ;',IDB, (err, result)=>{
        if(err){
            callback({Mensaje: 'Error al Mostrar datos', Result: false})
            throw err
        }else{
            callback(result)
        }
    })
}

isr.insert = (data, callback)=>{
    database.query('INSERT INTO isr(cuota, mes, anio,idEmpleado, fecha_creacion) VALUES (?,?,?,?,NOW()); ', data,
            (err, result)=>{
                if(err){
                    callback({Mensaje: 'Error al agregar el ISR', Result: false})
                    throw err
                }else{
                    callback({Mensaje: 'Agregando!', Result: true})
                }
            })
}

isr.delete = (IDE, callback)=>{
    database.query('DELETE FROM isr WHERE idISR = ?;', IDE, (err, result)=>{
        if(err){
            callback({Mensaje: 'Error al eliminar el ISR', Result: false})
            throw err
        }else{
            callback({Mensaje: 'Borrado!', Result: true})
        }
    })
}

module.exports = isr

