var database = require('./database'), detalleP = {}
/**
 * @param {number} IDP -ID Planilla
 * Obtiene los datos del detalle por planilla 
 */
detalleP.selectAll = (IDP, callback)=>{
    database.query(`SELECT detalleplanilla.*, DATE_FORMAT(empleado.fecha_inicio, '%d-%m-%Y') AS fecha_inicio FROM detalleplanilla 
    LEFT JOIN empleado ON empleado.idEmpleado = detalleplanilla.idEmpleado WHERE idPlanilla = ?;`, IDP, (err, res)=>{
        if(err){ 
            callback({Mensaje: 'Error al obtener los datos', Result: false})
            throw err
        }else{
            callback(res)
        }
    })
}
/**
 * @param {number} IDD - ID Detalle Planilla
 * Se obtienen solamente un detalle 
 */
detalleP.select = (IDD, callback)=>{
    database.query(`SELECT detalleplanilla.*, DATE_FORMAT(empleado.fecha_inicio, '%d-%m-%Y') AS fecha_inicio  FROM detalleplanilla 
    LEFT JOIN empleado ON empleado.idEmpleado = detalleplanilla.idEmpleado WHERE detalleplanilla.idDetalleP = ?;`, IDD, (err, res)=>{
        if(err){
            callback({Mensaje: 'Error al obtener los datos', Result: false})
            throw err
        }else{
            callback(res)
        }
    })
}

detalleP.selectTotales = (IDP, callback)=>{
    database.query(`SELECT SUM(dp.salario_liquido) AS total_planilla , SUM(dp.bonificacion) AS total_bonificacion, SUM(dp.retencion_isr) AS total_isr,
    SUM(dp.sueldo_ordinario) AS  total_sueldo_o, SUM(dp.salario_liquido) AS total_salario_l , SUM(dp.salario_base) AS total_salario_b,
    SUM(dp.igss) AS total_igss FROM detalleplanilla dp WHERE dp.idPlanilla = ?;`, IDP, (err, res)=>{
        if(err){
            callback({Mensaje: 'Error al obtener los datos', Result: false})
            throw err
        }else{
            callback(res)
        }
    })
}


/**
 * @param {Array} data - [nombre_empleado, bonificacion , retencion_isr , salario_liquido, salario_base , igss, idDetalleP ]
 * Edita los datos de un detalle de planilla
 */
detalleP.update = (data, callback)=>{
    database.query(`UPDATE detalleplanilla SET bonificacion = ?, retencion_isr = ?, 
                    salario_liquido = ? , salario_base = ?, igss = ?,sueldo_ordinario=?  WHERE idDetalleP = ?;`, data, (err, res)=>{
        if(err){
            callback({Mensaje: 'Ocurrio un error al editar', Result: false})
            throw err
        }else{
            callback({Mensaje: 'Editado!', Result: true})
        }
    })
}
/**
 * @param {number} IDD - ID Detalle Planilla
 * Elimina un detalle de la planilla
 */
detalleP.delete = (IDD, callback)=>{
    database.query('DELETE FROM detalleplanilla WHERE idDetalleP = ?', IDD, (err, res)=>{
        if(err){
            callback({Mensaje: 'Ocurrio un error al eliminar', Result: false})
            throw err
        }else{
            callback({Mensaje: 'Eliminar!', Result: true})
        }
    })
}

module.exports = detalleP