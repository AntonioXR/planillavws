var database = require('./database'), bonificacion = {}
/**
 * @param {number} IDE- ID Empleado
 * obtiene el historial de bonificaciones del empleado
 */
bonificacion.selectAll = (IDE, callback) =>{
    database.query('SELECT * ,DATE_FORMAT(fecha_creacion, "%Y-%m-%d") AS fecha_f  FROM bonificacion WHERE idEmpleado = ? ;', IDE, (err, result)=>{
        if(err){
            callback({Mensaje: 'Error al Mostrar datos', Result: false})
            throw err
        }else{
            callback(result)
        }
    })
}
/**
 * @param {number} IDB- ID Bonificacion
 * Obtiene solo una bonificacion
 */
bonificacion.select = (IDB, callback)=>{
    database.query('SELECT *, DATE_FORMAT(fecha_creacion, "%Y-%m-%d") AS fecha_f FROM bonificacion WHERE idBonificacion = ? ;', IDB, (err, result)=>{
        if(err){
            callback({Mensaje: 'Error al Mostrar datos', Result: false})
            throw err
        }else{
            callback(result)
        }
    })
}
/**
 * @param {Array} data- [cuota, fecha_creacion, idEmpleado]
 * Inserta una nueva bonificacion
 */
bonificacion.insert = (data, callback)=>{
    database.query('INSERT INTO bonificacion(cuota, mes, anio,idEmpleado, fecha_creacion) VALUES (?,?,?,?,NOW()); ', data,
            (err, result)=>{
                if(err){
                    callback({Mensaje: 'Error al agregar la bonificacion', Result: false})
                    throw err
                }else{
                    callback({Mensaje: 'Agregando!', Result: true})
                }
            })
}
/**
 * @param {number} IDE - ID Empleado
 * Eliminar una de las notificaciones del historial
 */
bonificacion.delete = (IDE, callback)=>{
    database.query('DELETE FROM bonificacion WHERE idBonificacion = ?;',IDE, (err, result)=>{
        if(err){
            callback({Mensaje: 'Error al eliminar la bonificacion', Result: false})
            throw err
        }else{
            callback({Mensaje: 'Borrado!', Result: true})
        }
    })
}

module.exports = bonificacion