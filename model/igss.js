var database = require('./database'), igss =  {}

igss.selectAll = (callback)=>{
    database.query('SELECT * FROM igss', (err, result)=>{
        if(err){
            callback({Result:false, Mensaje: 'Error en la consulta a la base de datos'})
            throw err;
        }else{
            callback(result)
        }
    })
}

igss.select = (ID,callback)=>{
    database.query('SELECT *, DATE_FORMAT(fecha_creacion, "%Y-%m-%d" ) AS fecha_f FROM igss WHERE idIGSS = ?',ID, (err, result)=>{
        if(err){
            callback([])
            throw err
        }else{
            callback(result)
        }
    })
}

igss.forYear = (year,callback)=>{
    database.query('SELECT * , DATE_FORMAT(fecha_creacion, "%Y-%m-%d" ) AS fecha_f FROM igss WHERE anio = ?', year, (err, result)=>{
        if(err){
            callback([])
            throw err
        }else{
            callback(result)
        }
    })
}

igss.insert =  (data, callback)=>{
    database.query('INSERT INTO igss(anio, cuota, fecha_creacion) VALUES (?,?, NOW());', data, (err, result)=>{
        if(err){
            callback({Result:false, Mensaje: 'Error al insertar datos'})
            throw err;
        }else{
            callback({Result:true, Mensaje: 'Listo'})
        }
    })
}

igss.update =  (data, callback)=>{
    database.query('UPDATE igss SET anio = ? , cuota = ? WHERE idIGSS = ?;', data, (err, result)=>{
        if(err){
            callback({Result:false, Mensaje: 'Error al editar los datos'})
            throw err;
        }else{
            callback({Result:true, Mensaje: 'Listo'})
        }
    })
}

igss.delete =  (data, callback)=>{
    database.query('DELETE FROM igss WHERE idIGSS = ?;', data, (err, result)=>{
        if(err){
            callback({Result:false, Mensaje: 'Error al eliminar'})
            throw err;
        }else{
            callback({Result:true, Mensaje: 'Listo'})
        }
    })
}

module.exports =  igss